Проект для тренировки новых разработчиков roboto.pro (roboto-pro-front-strapi-quasar)

roboto.pro frontend

[[_TOC_]]

# Установка фронта на компьютере разработчика
## Предварительная подготовка рабочего окружения
### Установка cURL
```shell
sudo apt install curl
```
curl - это утилита командной строки, которая позволяет работать с удалёнными серверами по различным протоколам, использующим URL для адресации ресурсов. Нам она нужна для скачивания установщика менеджера версий NodeJS.
### Установка NVM
NVM - NodeJS Version Manager
```shell
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```
Запустить NVM
```shell
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

Это менеджер для установки и использования различных версий NodeJS.
### Установка NodeJS
```shell
nvm install 14
```
В данный момент для сборки фронта используется 14 версия NodeJS
### Установка Yarn
```shell
npm install yarn
```
Для управления пакетами NodeJS и запуска сборок мы используем Yarn, а не NPM. NPM будет использован только один раз для установки Yarn.
### Создание директории для файлов сайта
Создайте директорию, в которой будут находится собранные файлы сайта и разместите в нём один файл для проверки доступности сайта
```shell
sudo mkdir -p /var/www/production/pro/roboto/www/
sudo touch /var/www/production/pro/roboto/www/index.html
sudo sh -c "echo 'Hello ROBOTO.PRO' > /var/www/production/pro/roboto/www/index.html"
```
### Создание локальной записи для доменного имени roboto.pro.local
Открыть в текстовой редакторе файл /etc/hosts
```shell
sudo nano /etc/hosts
```
Внести туда запись
```
127.0.0.1       roboto.pro.local
```
## Получение кода и сборка проекта
Получить код
```shell
git clone git@gitlab.com:teknokomo/roboto-pro-front-strapi-quasar.git roboto-pro-frontend
cd roboto-pro-frontend
```
Собрать проект
```shell
yarn install
npx quasar build
```
Переместить сборку в директорию сайта roboto.pro.local
```shell
rm -rf /var/www/production/pro/roboto/www/*
cp -r dist/spa/* /var/www/production/pro/roboto/www/
```
Проверить сборку
```shell
x-www-browser http://roboto.pro.local
```
## Вариант с локальным Nginx
### Установка и настройка Nginx
Nginx - веб-сервер. Он будет отдавать вам страницу с вашим фронтом.
```shell
sudo apt install nginx
```
Создайте файл конфигурации вашего локального сайта roboto.pro.local
```shell
sudo touch /etc/nginx/sites-available/roboto.pro.local
```
Откройте файл в текстовом редакторе
```
sudo nano /etc/nginx/sites-available/roboto.pro.local
```
Поместите в него следующую конфигурацию
```nginx
server {
    listen 80;
    server_name roboto.pro.local;
    root /var/www/production/pro/roboto/www/;
    index index.html;
}
```
Сделайте ссылку на этот файл в директории доступных сайтов
```shell
sudo ln -s /etc/nginx/sites-available/roboto.pro.local /etc/nginx/sites-enabled/roboto.pro.local
```
Перезапустите Nginx для использования новой конфигурации
```shell
sudo systemctl restart nginx
```
Открыть в браузере по умолчанию URL http://roboto.pro.local . Браузер должен вывести строку "Hello ROBOTO.PRO".
```shell
x-www-browser http://roboto.pro.local
```
## Вариант с Docker
Установить Docker
```shell
sudo apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
Собрать SPA-приложение в контейнере. После сборки контейнер сам удалиться.
```shell
docker run \
  --name roboto-pro-frontend-quasar-build \
  -it \
  --rm \
  -v $(pwd):/app \
  -w /app \
  -e API_URI='https://roboto.pro:1338' \
  -e GRAPHQL_URI='https://roboto.pro:1338/graphql' \
  node:16-bullseye \
  /bin/bash -c "npm install yarn && yarn install && npx quasar build"
```
Запустить контейнер с Nginx на порту 3000 с примонтированными файлами сборки
```shell
docker run \
  --name roboto-pro-frontend-quasar \
  -v $(pwd)/dist/spa:/usr/share/nginx/html \
  -e NGINX_HOST=roboto.pro.local \
  -p 3000:80 \
  -d \
  nginx
```
Открыть в браузере по умолчанию URL http://roboto.pro.local . Браузер должен вывести строку "Hello ROBOTO.PRO".
```shell
x-www-browser http://roboto.pro.local:3000
```
# Установка фронта на рабочем сервере
## Требования к серверу Jenkins
железо:
- RAM от 2GB

пакеты:
- jenkins
- ansible
- python3
- sshpass

## Сборка
На сервере с Jenkins надо установить Ansible.<br>
Иначе при сборке будет получена ошибка:
```
Cannot run program "ansible-playbook" (in directory "/var/lib/jenkins/workspace/RobotoProPipeline"): error=2, No such file or directory
```
На сервере должен быть установлен Nginx, Certbot
## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Сборка и развёртывание сайта
В данный момент всё происходит автоматически. В репозитории настроен вебхук, который
дёргает Jenkins, тот скачивает репозиторий, собирает его, создаёт архив из собранных файлов,
с помощью Ansible перекидывает архив на целевой сервер и разворачивает его в папке, за которой
следит Nginx.

Сценарии для конвейера находятся в папке pipeline.

Для конвейера в Jenkins должны быть установлены следующие плагины (помимо стандартных):
- GitLab Plugin - позволяет создавать вебхуки для GitLab
- NodeJS Plugin - нужен для сборки проекта при помощи NPM или YARN
- Ansible Plugin - занимается развёртыванием артефактов (поднимает сайт) на целевом сервере
